﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MegaChallengeCasino
{
    public partial class _default : System.Web.UI.Page
    {
        // Display 3 random images from 12 images when Page is loaded and push the button.
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState.Add("Money", 100);
                ViewState.Add("Random", new Random());
                displayInit();
            }
        }

        protected void leverButton_Click(object sender, EventArgs e)
        {
            doCasinoGame();
        }

        private int getBettedMoney()
        {
            int bettedMoney;
            if (!int.TryParse(betTextBox.Text.Trim(), out bettedMoney))
                return 0;

            return bettedMoney;
        }
        private void doCasinoGame()
        {
            int bettedMoney = getBettedMoney();
            int playerMoney = (int)ViewState["Money"];
            if ((bettedMoney > playerMoney) || (bettedMoney == 0)) return;

            playerMoney += playGame(bettedMoney); 
            ViewState["Money"] = playerMoney;
            displayMoney(playerMoney);
        }


        private int playGame(int bettedMoney)
        {
            int[] items = new int[3];
            runMachine(items);
            displayImages(items);
            int earnMoney = judgeBet(items, bettedMoney);
            displayResult(bettedMoney, earnMoney);

            return earnMoney;
        }
        private void runMachine(int[] items)
        {
            Random random = (Random)ViewState["Random"];

            for (int i = 0; i < 3; i++)
                items[i] = random.Next(0, 11);

            ViewState["Random"] = random;
        }


        private int judgeBet(int[] items, int bettedMoney)
        {
            int cherryCount = 0, sevenCount = 0, barCount = 0;

            for (int i = 0; i < items.Length; i++)
                if (items[i] == 2) cherryCount++;// equal to Cherry
                else if (items[i] == 9) sevenCount++;// equal to Seven
                else if (items[i] == 0) barCount++;// equal to bar

            return bettedMoney * getMultiplier(cherryCount, sevenCount, barCount);
        }

        private int getMultiplier(int cherryCount, int sevenCount, int barCount)
        {
            if (barCount > 0) return -1;
            if (sevenCount == 3) return 100;  // Jackpot !!!
            if (cherryCount > 0) return cherryCount + 1;

            return -1;
        }

        private void displayImages(int[] items)
        {
            string[] images = {
                "Bar.png", "Bell.png", "Cherry.png", "Clover.png", "Diamond.png", "HorseShoe.png",
                "Lemon.png", "Orange.png", "Plum.png", "Seven.png", "Strawberry.png", "Watermellon.png"};

            Image1.ImageUrl = images[items[0]];
            Image2.ImageUrl = images[items[1]];
            Image3.ImageUrl = images[items[2]];
        }

        private void displayMoney(int playerMoney)
        {
            moneyLabel.Text = string.Format("Player's Money: ${0:N2}", playerMoney);
        }
        private void displayResult(int bettedMoney, int earnMoney)
        {
            if (earnMoney > bettedMoney)
                resultLabel.Text = string.Format("You bet ${0:N2} and won ${1:N2}!", bettedMoney, earnMoney);
            else
                resultLabel.Text = string.Format("Sorry, you lost ${0:N2}. Better luck next time.", bettedMoney);
        }
        private void displayInit()
        {
            int[] items = new int[3];
            runMachine(items);
            displayImages(items);
            displayMoney(100);
        }

    }
}