﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PapaBobsMegaChallenge.Persistence
{
    public class PriceRepository
    {
        static private PriceRepository _priceRepository = null;
        static public PriceRepository GetInstance()
        {
            if (_priceRepository == null)
                _priceRepository = new PriceRepository();

            return _priceRepository;
        }

        public DTO.Price GetPizzaPrice()
        {
            DTO.Price pizzaPrice = new DTO.Price();

            pizzaPrice.SmallSize = 12;
            pizzaPrice.MediumSize = 14;
            pizzaPrice.LargeSize = 16;

            pizzaPrice.ReguralCrust = 0;
            pizzaPrice.ThinCrust = 0;
            pizzaPrice.ThickCrust = 2;

            pizzaPrice.SausageTopping = 2;
            pizzaPrice.PepperoniTopping = 1.5;
            pizzaPrice.OnionsTopping = 1;
            pizzaPrice.GreenPepperTopping = 1;

            return pizzaPrice;
        }

        public double GetPriceOfOrder(DTO.Order order)
        {
            double price = 0;

            price += GetPriceWithSize(order.Size);
            price += GetAddedPriceWithCrust(order.Crust);
            price += GetAddedPriceWithTopping(order.Sausage, order.Pepperoni, order.Onions, order.GreenPeppers);

            order.TotalCost = price;

            return price;
        }

        public double GetPriceWithSize(DTO.PizzaSize size)
        {
            double price = 0.0;

            switch (size)
            {
                case DTO.PizzaSize.Medium:
                    price = 14;
                    break;
                case DTO.PizzaSize.Large:
                    price = 16;
                    break;
                default:
                    price = 12;
                    break;
            }

            return price;
        }

        public double GetAddedPriceWithCrust(DTO.PizzaCrust crust)
        {
            double price = 0.0;

            switch (crust)
            {
                case DTO.PizzaCrust.Thick:
                    price = 2.0;
                    break;
                default:
                    price = 0.0;
                    break;
            }

            return price;
        }

        public double GetAddedPriceWithTopping(bool sausage, bool pepperoni, bool oninos, bool greenPeppers)
        {
            double price = 0.0;

            if (sausage) price += 2;
            if (pepperoni) price += 1.5;
            if (oninos) price += 1;
            if (greenPeppers) price += 1;

            return price;
        }
    }
}
