﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PapaBobsMegaChallenge;

namespace PapaBobsMegaChallenge.Persistence
{
    public class CustomerRepository
    {
        static private CustomerRepository _customerRepository = null;
        private OrderManagementEntities _db = null;

        static public CustomerRepository GetInstance()
        {
            if (_customerRepository == null)
            {
                _customerRepository = new CustomerRepository();
            }

            return _customerRepository;
        }

        public CustomerRepository()
        {
            _db = new OrderManagementEntities();
        }

        public void CompleteOrder(Guid orderID)
        {
            var dbOrder = _db.Table.Find(orderID);

            dbOrder.Complete = true;

            _db.SaveChanges();
        }


        public void AddNewOrder(DTO.Order dtoOrder)
        {
            checkValidity(dtoOrder);

            var dbOrder = ConvertFormatFromDTOtoDB(dtoOrder);

            _db.Table.Add(dbOrder);
            _db.SaveChanges();
        }

        private void checkValidity(DTO.Order dtoOrder)
        {
            if (dtoOrder.Name == null || dtoOrder.Name.Trim().Length == 0)
                throw new ArgumentException("Please let you check Name field. You should fill this.");
            if (dtoOrder.Address == null || dtoOrder.Address.Trim().Length == 0)
                throw new ArgumentException("Please let you check Address field. You should fill this.");
            if (dtoOrder.Zip == 0)
                throw new ArgumentException("Please let you check Zip code field. You should fill this.");
            if (dtoOrder.Phone == null || dtoOrder.Phone.Trim().Length == 0)
                throw new ArgumentException("Please let you check Phone number field. You should fill this.");
        }

        public List<DTO.Order> GetOrders()
        {
            var dbOrders = _db.Table.ToList();
            var dtoOrders = new List<DTO.Order>();

            foreach (var dbOrder in dbOrders)
            {
                dtoOrders.Add(ConvertFormatFromDBtoDTO(dbOrder));
            }

            return dtoOrders;
        }

        private DTO.Order ConvertFormatFromDBtoDTO(Table dbOrder)
        {
            DTO.Order dtoOrder = new DTO.Order();

            dtoOrder.OrderId = dbOrder.OrderId;
            dtoOrder.Size = (DTO.PizzaSize)dbOrder.Size;
            dtoOrder.Crust = (DTO.PizzaCrust)dbOrder.Crust;
            dtoOrder.Sausage = dbOrder.Sausage;
            dtoOrder.Pepperoni = dbOrder.Pepperoni;
            dtoOrder.Onions = dbOrder.Onions;
            dtoOrder.GreenPeppers = dbOrder.GreenPeppers;
            dtoOrder.TotalCost = dbOrder.TotalCost;
            dtoOrder.Name = dbOrder.Name;
            dtoOrder.Address = dbOrder.Address;
            dtoOrder.Zip = dbOrder.Zip;
            dtoOrder.Phone = dbOrder.Phone;
            dtoOrder.Payment = (DTO.Payment)dbOrder.Payment;
            dtoOrder.Completed = dbOrder.Complete;

            return dtoOrder;
        }

        private Table ConvertFormatFromDTOtoDB(DTO.Order dtoOrder)
        {
            Table dbOrder = new Table();

            dbOrder.OrderId = Guid.NewGuid();
            dbOrder.Size = (int)dtoOrder.Size;
            dbOrder.Crust = (int)dtoOrder.Crust;
            dbOrder.Sausage = dtoOrder.Sausage;
            dbOrder.Pepperoni = dtoOrder.Pepperoni;
            dbOrder.Onions = dtoOrder.Onions;
            dbOrder.GreenPeppers = dtoOrder.GreenPeppers;
            dbOrder.TotalCost = dtoOrder.TotalCost;
            dbOrder.Name = dtoOrder.Name;
            dbOrder.Address = dtoOrder.Address;
            dbOrder.Zip = dtoOrder.Zip;
            dbOrder.Phone = dtoOrder.Phone;
            dbOrder.Payment = (int)dtoOrder.Payment;
            dbOrder.Complete = dtoOrder.Completed;

            return dbOrder;
        }

    }
}
