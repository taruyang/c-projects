﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderManagement.aspx.cs" Inherits="PapaBobsMegaChallenge.Presentation.OrderManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>Order Management</h1>

        <br />
        <br />
    
        <asp:GridView ID="orderManagementGridView" runat="server" CssClass="table-responsive" OnRowCommand="orderManagementGridView_RowCommand" OnSelectedIndexChanged="orderManagementGridView_SelectedIndexChanged">
            <Columns>
                <asp:ButtonField CommandName="Select" Text="Finished" />
            </Columns>
        </asp:GridView>
    
    </div>
    <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    </form>

</body>
</html>
