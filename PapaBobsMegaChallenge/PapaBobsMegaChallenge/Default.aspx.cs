﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PapaBobsMegaChallenge;

namespace PapaBobsMegaChallenge.Presentation
{
    public partial class Default : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                DTO.Order newOrder = new DTO.Order();
                double price = Domain.CustomerManager.GetInstance().Calcurate(newOrder);
                DisplayTotalCost(price);

                ViewState.Add("NewOrder", newOrder);
            }
        }

        public void DisplayTotalCost(double cost)
        {
            resultLabel.Text = String.Format("${0}", cost);
        }

        public void DisplayErrorMessage(string error)
        {
            errorLabel.Text = error;
        }

        protected void orderButton_Click(object sender, EventArgs e)
        {
            DTO.Order newOrder = (DTO.Order)ViewState["NewOrder"];
            getDeliveryInformation(newOrder);
            getPaymentInformation(newOrder);

            try
            {
                Domain.CustomerManager.GetInstance().AddNewOrder(newOrder);

            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex.Message); return;
            }

            Server.Transfer("Success.aspx");
        }

        private void getDeliveryInformation(DTO.Order order)
        {
            order.Name = nameTextBox.Text;
            order.Address = addressTextBox.Text;
            if (zipTextBox.Text.Trim().Length == 0)
                order.Zip = 0;
            else
                order.Zip = int.Parse(zipTextBox.Text);
            order.Phone = phoneTextBox.Text;
        }

        private void getPaymentInformation(DTO.Order order)
        {
            if (cashRadioButton.Checked)
                order.Payment = DTO.Payment.Cash;
            else
                order.Payment = DTO.Payment.Credit;
        }

        protected void sizeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DTO.Order newOrder = (DTO.Order)ViewState["NewOrder"];
            newOrder.Size = (DTO.PizzaSize)sizeDropDownList.SelectedIndex;
            double price = Domain.CustomerManager.GetInstance().Calcurate(newOrder);
            DisplayTotalCost(price);
        }

        protected void crustDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DTO.Order newOrder = (DTO.Order)ViewState["NewOrder"];
            newOrder.Crust = (DTO.PizzaCrust)crustDropDownList.SelectedIndex;
            double price = Domain.CustomerManager.GetInstance().Calcurate(newOrder);
            DisplayTotalCost(price);
        }

        protected void sausageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            DTO.Order newOrder = (DTO.Order)ViewState["NewOrder"];
            newOrder.Sausage = sausageCheckBox.Checked;
            double price = Domain.CustomerManager.GetInstance().Calcurate(newOrder);
            DisplayTotalCost(price);
        }

        protected void pepperoniCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            DTO.Order newOrder = (DTO.Order)ViewState["NewOrder"];
            newOrder.Pepperoni = pepperoniCheckBox.Checked;
            double price = Domain.CustomerManager.GetInstance().Calcurate(newOrder);
            DisplayTotalCost(price);
        }

        protected void onionsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            DTO.Order newOrder = (DTO.Order)ViewState["NewOrder"];
            newOrder.Onions = onionsCheckBox.Checked;
            double price = Domain.CustomerManager.GetInstance().Calcurate(newOrder);
            DisplayTotalCost(price);
        }

        protected void greenPeppersCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            DTO.Order newOrder = (DTO.Order)ViewState["NewOrder"];
            newOrder.GreenPeppers = greenPeppersCheckBox.Checked;
            double price = Domain.CustomerManager.GetInstance().Calcurate(newOrder);
            DisplayTotalCost(price);
        }


    }
}