﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PapaBobsMegaChallenge.Presentation.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style2 {
            font-size: x-large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>Papa Bobs Pizza</h1>
        <p class="lead">Pizza to Code By</p>
        <hr />

        <div class="form-group">
            <label>Size:</label>
            <asp:DropDownList ID="sizeDropDownList" runat="server" CssClass="form-control" OnSelectedIndexChanged="sizeDropDownList_SelectedIndexChanged" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="Small">Small (12 inch - $12)</asp:ListItem>
                <asp:ListItem Value="Medium">Medium (14 inch - $14)</asp:ListItem>
                <asp:ListItem Value="Large">Large (16 inch - $16)</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <label>Crust:</label>
            <asp:DropDownList ID="crustDropDownList" runat="server" CssClass="form-control" OnSelectedIndexChanged="crustDropDownList_SelectedIndexChanged" AutoPostBack="True">
            <asp:ListItem Selected="True" Value="Regular">Regular</asp:ListItem>
            <asp:ListItem Value="Thin">Thin</asp:ListItem>
            <asp:ListItem Value="Thick">Thick (+ $2)</asp:ListItem>
            </asp:DropDownList>
        </div>

        <div class="checkbox">
            <label>
                <asp:CheckBox ID="sausageCheckBox" runat="server" OnCheckedChanged="sausageCheckBox_CheckedChanged" AutoPostBack="True" /> Sausage (+ $2)
            </label>
        </div>
        <div class="checkbox">
            <label>
                <asp:CheckBox ID="pepperoniCheckBox" runat="server" OnCheckedChanged="pepperoniCheckBox_CheckedChanged" AutoPostBack="True" /> Pepperoni (+ $1.50)
            </label>
        </div>
        <div class="checkbox">
            <label>
                <asp:CheckBox ID="onionsCheckBox" runat="server" OnCheckedChanged="onionsCheckBox_CheckedChanged" AutoPostBack="True" /> Onions (+ $1)
            </label>
        </div>
        <div class="checkbox">
            <label>
                <asp:CheckBox ID="greenPeppersCheckBox" runat="server" OnCheckedChanged="greenPeppersCheckBox_CheckedChanged" AutoPostBack="True" /> Green Peppers (+ $1)
            </label>
        </div>
        
        <h2>Deliver To:</h2>
        <div class="form-group">
            <label>Name:</label>
            <asp:TextBox ID="nameTextBox" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <label>Address:</label>
            <asp:TextBox ID="addressTextBox" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <label>Zip:</label>
            <asp:TextBox ID="zipTextBox" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <label>Phone:</label>
            <asp:TextBox ID="phoneTextBox" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

        <h2>Payment:</h2>
        <div class="radio">
            <label>
                <asp:RadioButton ID="cashRadioButton" runat="server" GroupName="Payment" Checked="True"/>
                Cash
            </label>
        </div>
        <div class="radio">
            <label>
                <asp:RadioButton ID="creditButton" runat="server" GroupName="Payment"/>
                Credit
            </label>
        </div>
        <asp:Button ID="orderButton" runat="server" Text="Order" CssClass="btn btn-lg btn-primary" OnClick="orderButton_Click" /><br />
        <asp:Label ID="errorLabel" runat="server" CssClass="control-label" Font-Size="Small" ForeColor="#FF6600"></asp:Label>
        <br />

        <span class="auto-style2">Total Cost:
        </span>
        <asp:Label ID="resultLabel" runat="server" Text="Test" Font-Size="X-Large"></asp:Label>
        
    </div>
    <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    </form>
        
</body>
</html>
<script src="scripts/jquery-1.9.1.min.js"></script>
