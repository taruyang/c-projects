﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PapaBobsMegaChallenge;

namespace PapaBobsMegaChallenge.Presentation
{
    public partial class OrderManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Domain.CustomerManager customerManager = Domain.CustomerManager.GetInstance();
                var orders = customerManager.GetOrders();
                orderManagementGridView.DataSource = orders.FindAll(p => p.Completed == false);
                orderManagementGridView.DataBind();
            }
        }

        protected void orderManagementGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void orderManagementGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Domain.CustomerManager customerManager = Domain.CustomerManager.GetInstance();
            var orderID = getSelectedOrderID(e);
            customerManager.CompleteOrder(orderID);
            orderManagementGridView.DataSource = customerManager.GetOrders().FindAll(p => p.Completed == false);
            orderManagementGridView.DataBind();
        }

        Guid getSelectedOrderID(GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = orderManagementGridView.Rows[index];
            var orderID = Guid.Parse(row.Cells[1].Text);

            return orderID;
        }
    }
}