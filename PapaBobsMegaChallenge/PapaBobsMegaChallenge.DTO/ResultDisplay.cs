﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PapaBobsMegaChallenge.DTO
{
    public delegate void TotalCostCallback(string cost);
    public delegate void ErrorCallback(string error);

    public class ResultDisplay
    {
        public TotalCostCallback DisplayTotalCost { get; set; }
        public ErrorCallback DisplayError { get; set; }

        public ResultDisplay(TotalCostCallback totalCostCallback, ErrorCallback errorCallback)
        {
            DisplayTotalCost = totalCostCallback;
            DisplayError = errorCallback;
        }
    }
}
