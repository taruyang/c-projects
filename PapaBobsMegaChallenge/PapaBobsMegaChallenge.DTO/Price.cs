﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PapaBobsMegaChallenge.DTO
{
    public class Price
    {
        // Size
        public double SmallSize { get; set; }
        public double MediumSize { get; set; }
        public double LargeSize { get; set; }

        // Crust
        public double ReguralCrust { get; set; }
        public double ThinCrust { get; set; }
        public double ThickCrust { get; set; }

        // Topping
        public double SausageTopping { get; set; }
        public double PepperoniTopping { get; set; }
        public double OnionsTopping { get; set; }
        public double GreenPepperTopping { get; set; }
    }
}
