﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PapaBobsMegaChallenge.DTO
{
    [Serializable()]
    public  class Order
    {
        public Guid OrderId { get; set; }
        public PizzaSize Size { get; set; }
        public PizzaCrust Crust { get; set; }
        public bool Sausage { get; set; }
        public bool Pepperoni { get; set; }
        public bool Onions { get; set; }
        public bool GreenPeppers { get; set; }
        public double TotalCost { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int Zip { get; set; }
        public string Phone { get; set; }
        public Payment Payment { get; set; }
        public bool Completed { get; set; }
    }

    public enum PizzaSize
    {
        Small,
        Medium,
        Large
    }

    public enum PizzaCrust
    {
        Regular,
        Thin,
        Thick
    }

    public enum Payment
    {
        Cash,
        Credit
    }
}
