﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PapaBobsMegaChallenge;

namespace PapaBobsMegaChallenge.Domain
{
    public class CustomerManager
    {
        static private CustomerManager _customerManager = null;

        private Persistence.CustomerRepository _customerRepository = null;
        private Persistence.PriceRepository _priceRepository = null;

        static public CustomerManager GetInstance()
        {
            if (_customerManager == null)
                _customerManager = new CustomerManager();

            return _customerManager;
        }

        public CustomerManager()
        {
            _customerRepository = Persistence.CustomerRepository.GetInstance();
            _priceRepository = Persistence.PriceRepository.GetInstance();
        }

        public List<DTO.Order> GetOrders()
        {
            return _customerRepository.GetOrders();
        }

        public void CompleteOrder(Guid orderID)
        {
            _customerRepository.CompleteOrder(orderID);
        }

        public void AddNewOrder(DTO.Order dtoOrder)
        {
            try
            {
                _customerRepository.AddNewOrder(dtoOrder);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        public double Calcurate(DTO.Order dtoOrder)
        {
            double calcuratedValue = 0.0;

            calcuratedValue = _priceRepository.GetPriceOfOrder(dtoOrder);

            return calcuratedValue;
        }
    }
}
