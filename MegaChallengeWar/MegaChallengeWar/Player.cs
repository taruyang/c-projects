﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MegaChallengeWar
{
    public class Player
    {
        public int PlayerNum { get; set; }
        public Deck MyBounty { get; set; }
        public Deck MyDeck { get; set; }

        public Player(int playerNum)
        {
            PlayerNum = playerNum;
            MyBounty = new MegaChallengeWar.Deck();
            MyDeck = new Deck();
        }

        public List<Card> PopCards(int numberOfCards)
        {
            return MyDeck.PopCards(numberOfCards);
        }
    }
}