﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MegaChallengeWar
{
    public class Game
    {
        Player _playerOne;
        Player _playerTwo;
        Dealer _dealer;

        public Game(Dealer dealer, Player playerOne, Player playerTwo)
        {
            _dealer = dealer;
            _playerOne = playerOne;
            _playerTwo = playerTwo;
        }

        public string PrepareGame()
        {
            string result = "<h2> Dealing cards ... </h2></br>";
            result += _dealer.DealCardsToPlayers(_playerOne, _playerTwo);
            return result;
        }

        public string Battle(int numberOfTern = 10)
        {
            string result = "<h2> Begin battle ... </h2></br>";

            for (int i = 0; i < numberOfTern; i++)
            {
                result += oneBattle();
                if (_dealer.EndOfGame) break;
            }

            result += _dealer.FinalJudge(_playerOne.MyDeck, _playerTwo.MyDeck);
            return result;
        }

        private string oneBattle()
        {
            string result = "";
            int numberOfCards = _dealer.NextTurnCardsCount; 

            result += _dealer.JudgeCards(_playerOne.PopCards(numberOfCards), _playerTwo.PopCards(numberOfCards));
            _dealer.GiveCardsToWinner(_playerOne.MyDeck, _playerTwo.MyDeck);

            return result;
        }
    }
}