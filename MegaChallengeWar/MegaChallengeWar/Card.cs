﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MegaChallengeWar
{
    public class Card
    {
        public int Number { get; set; }
        public string AllocatedSet { get; set; }    // Clubs, Diamonds, Spades, Hearts
        override public string ToString()
        {
            string mark;
            if (Number == 1) mark = "Ace";
            else if (Number == 11) mark = "Jack";
            else if (Number == 12) mark = "Queen";
            else if (Number == 13) mark = "King";
            else mark = Number.ToString();

            return String.Format("{0} of {1}", mark, AllocatedSet);
        }
    }
}