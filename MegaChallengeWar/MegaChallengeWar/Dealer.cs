﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MegaChallengeWar
{
    public enum ResultOfBattle
    {
        None,
        PlayerOne,
        PlayerTwo,
        War
    }

    public class Dealer
    {
        private Random _random = new Random();
        private ResultOfBattle _resultOfBattle;
        private List<Card> _cardsOnTable = new List<Card>();

        public int NextTurnCardsCount { get; set; } = 1;
        public bool EndOfGame { get; set; }

        public string DealCardsToPlayers(Player playerOne, Player playerTwo)
        {
            Deck deck = makeInitialDeck();
            string result = "";

            while (deck.Cards.Count != 0)
            {
                result += dealOneCard(playerOne, extractOneCardFromDeck(deck));
                result += dealOneCard(playerTwo, extractOneCardFromDeck(deck));
            }

            return result;
        }

        private Deck makeInitialDeck()
        {
            string[] sets = { "Clubs", "Diamonds", "Spades", "Hearts" };

            Deck initialDeck = new Deck();
            foreach (var set in sets)
                for (int i = 2; i < 15; i++)
                    initialDeck.Cards.Add(new Card { Number = i, AllocatedSet = set });

            return initialDeck;
        }

        internal string FinalJudge(Deck playerOneDeck, Deck playerTwoDeck)
        {
            int winPlayer = 1;
            if (playerTwoDeck.Cards.Count > playerOneDeck.Cards.Count) winPlayer = 2;

            return String.Format("<h2></br>Player{0} wins </br>Player1:{1} </br>Player2:{2}</h2>",
                winPlayer,
                playerOneDeck.Cards.Count,
                playerTwoDeck.Cards.Count);
        }

        private Card extractOneCardFromDeck(Deck deck)
        {
            Card extractedCard = deck.Cards[_random.Next(deck.Cards.Count)];
            deck.Cards.Remove(extractedCard);

            return extractedCard;
        }

        public string JudgeCards(List<Card> cardsOfPlayerOne, List<Card> cardsOfPlayerTwo)
        {
            evaluateCards(cardsOfPlayerOne, cardsOfPlayerTwo);
            arrageCardsOnTable(cardsOfPlayerOne, cardsOfPlayerTwo);
            return getResultOfJudge(cardsOfPlayerOne.Last(), cardsOfPlayerTwo.Last());
        }

        private void evaluateCards(List<Card> cardsOfPlayerOne, List<Card> cardsOfPlayerTwo)
        {
            if (cardsOfPlayerOne.Count == 0) { _resultOfBattle = ResultOfBattle.PlayerTwo; EndOfGame = true; }
            else if (cardsOfPlayerTwo.Count == 0) { _resultOfBattle = ResultOfBattle.PlayerOne; EndOfGame = true; }
            else if (cardsOfPlayerOne.Last().Number > cardsOfPlayerTwo.Last().Number) _resultOfBattle = ResultOfBattle.PlayerOne;
            else if (cardsOfPlayerTwo.Last().Number > cardsOfPlayerOne.Last().Number) _resultOfBattle = ResultOfBattle.PlayerTwo;
            else { _resultOfBattle = ResultOfBattle.War; NextTurnCardsCount = 3; }
        }

        private void arrageCardsOnTable(List<Card> cardsOfPlayerOne, List<Card> cardsOfPlayerTwo)
        {
            _cardsOnTable.AddRange(cardsOfPlayerOne);
            _cardsOnTable.AddRange(cardsOfPlayerTwo);
        }

        private string getResultOfJudge(Card cardOfPlayerOne, Card cardOfPlayerTwo)
        {
            string result = String.Format("Battle Cards: {0} versus {1} </br>Bounty ... </br>", cardOfPlayerOne.ToString(), cardOfPlayerTwo.ToString());
            if (_resultOfBattle == ResultOfBattle.War) return result += "****************WAR****************</br></br>";

            foreach (var card in _cardsOnTable) result += String.Format("{0} </br>", card.ToString());

            if (_resultOfBattle == ResultOfBattle.PlayerOne) result += "Player1 wins! </br></br> ";
            else result += "Player2 wins! </br></br>";

            return result;
        }

        internal void GiveCardsToWinner(Deck playerOneDeck, Deck playerTwoDeck)
        {
            if (_resultOfBattle == ResultOfBattle.War) return;

            if (_resultOfBattle == ResultOfBattle.PlayerOne) playerOneDeck.PushCards(_cardsOnTable);
            else playerOneDeck.PushCards(_cardsOnTable);

            _cardsOnTable.Clear();
            _resultOfBattle = ResultOfBattle.None;
            NextTurnCardsCount = 1;
        }


        private string dealOneCard(Player player, Card card)
        {
            player.MyDeck.PushCard(card);
            return String.Format("Player{0} is dealt the {1} </br>", player.PlayerNum, card.ToString());
        }



    }
}