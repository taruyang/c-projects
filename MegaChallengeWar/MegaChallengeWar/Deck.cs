﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MegaChallengeWar
{
    public class Deck
    {
        public List<Card> Cards { get; set; }

        public Deck()
        {
            Cards = new List<Card>();
        }

        public void PushCard(Card card)
        {
            Cards.Add(card);
        }

        public void PushCards(List<Card> cards)
        {
            Cards.AddRange(cards);
        }

        public List<Card> PopCards(int numberOfCards)
        {
            if (numberOfCards > Cards.Count)
                return new List<Card>();
            List<Card> returnCards = Cards.GetRange(0, numberOfCards);
            Cards.RemoveRange(0, numberOfCards);
            
            return returnCards;
        }
    }
}